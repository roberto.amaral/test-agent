describe('Form Manager Page Tests', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/pos/order-management');

        cy.get('input[name=new-email]').type('roberto.amaral@arcca.io');

        cy.get('input[name=new-password]').type('bb556655');

        cy.get('button[type=submit]').click();

        cy.url().should('include', '/pos/order-management');

        cy.visit('http://localhost:3000/pos/order-management');
    });

    it('should allow creating a new order', () => {
        cy.contains('Em preparo').as('ordersLoaded');

        cy.wait('@ordersLoaded').then(() => {
            if (cy.contains('Em preparo') && !cy.contains('Fechado')) {
                cy.get('.create-order-button').click();

                cy.contains('Selecione um cliente').should('be.visible');

                // Wait for all APIs to be called and components to be rendered
                cy.wait('@api1');
                cy.wait('@api2');
                // Add more wait commands for other APIs if needed

                // Interact with the form creation dialog
                // For example, if you need to choose a type of form before saving
                // cy.get('v-radio a-radio-button theme--light v-item--active').click();

                /* cy.get('button').contains('Salvar').click();

                cy.url().should('eq', 'http://localhost:3000/form-builder/create-form?type=1'); */
            } else {
                // Handle the case when the API response does not contain the expected message
                // For example, show an error message or skip the test
                cy.log('API response does not contain the expected message');
            }
        });
    });
});