describe('Form Manager Page Tests', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/form-builder');

    cy.get('input[name=new-email]').type('roberto.amaral@arcca.io');

    cy.get('input[name=new-password]').type('bb556655');

    cy.get('button[type=submit]').click();

    cy.url().should('include', '/form-builder');

    cy.visit('http://localhost:3000/form-builder');
  });

  it('should display the page title correctly', () => {
    cy.contains('Gerenciar formulários').should('be.visible');
  });

  it('should allow creating a new form', () => {
    cy.get('.add-button').click();
    
    cy.contains('Qual tipo de formulário deseja criar?').should('be.visible');

    // Interact with the form creation dialog
    // For example, if you need to choose a type of form before saving
    // cy.get('v-radio a-radio-button theme--light v-item--active').click();

    // Click on the "salvar" button to create the form
    cy.get('button').contains('Salvar').click();

    // Wait for the redirection to happen after saving
    cy.url().should('eq', 'http://localhost:3000/form-builder/create-form?type=1');
  });

  /* it('should allow filling and saving a form', () => {
  
    // Fill out the form fields. Replace 'input-field-name' with the actual field names or selectors.
    cy.get('input[name="form-title"]').type('Form teste cypress');
    cy.get('textarea[name="form-description"]').type('Description for my new form');
    
    // If there are dropdowns or select fields, you would interact with them like this:
    cy.get('select[name="form-category"]').select('Category 1');
  
    // If there are radio buttons or checkboxes, you would interact with them like this:
    cy.get('input[type="radio"][value="Option 1"]').check();
    cy.get('input[type="checkbox"][name="feature"]').check(); // Checks all checkboxes with name "feature"
  
    // Before clicking save, intercept the API call if you want to mock the response or wait for the call to complete.
    cy.intercept('POST', '/api/form/save', { statusCode: 200, body: { id: '123' } }).as('saveForm');
  
    // Click the save button, replace '.save-button' with the actual selector for your save button.
    cy.get('.save-button').click();
  
    // Wait for the save form API call to complete.
    cy.wait('@saveForm');
  
    // Check the URL or page content to confirm the form was saved successfully.
    cy.url().should('include', '/form-builder/success'); // Replace with the URL you expect to redirect to after saving.
  
    // Optionally, assert a success message or the presence of the new form in a list.
    cy.contains('Form saved successfully').should('be.visible'); // Replace with actual success message.
    // cy.contains('My New Form').should('exist'); // To check if the new form is now listed somewhere.
  }); */
  
});
