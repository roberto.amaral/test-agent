// ***********************************************************
// This example support/component.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

import { mount } from 'cypress/vue2'

Cypress.Commands.add('mount', mount)
Cypress.Commands.add('login', () => {
    cy.request('POST', 'https://api-stg.arcca.io/api/authentication/login/', {
          username: 'roberto.amaral@arcca.io',
          password: 'bb556655',
        }).then((resp) => {
          window.localStorage.setItem('token', resp.body.token);
        });
      });

// Example use:
// cy.mount(MyComponent)