const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'i5tky8',
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },

  component: {
    devServer: {
      baseUrl: "http://localhost:3000",
      framework: "nuxt",
      bundler: "webpack",
    },
  },
});
