Cypress.Commands.add('login', () => {
    cy.request('POST', 'http://localhost:3000/api/login', {
      username: 'your_username',
      password: 'your_password',
    }).then((resp) => {
      window.localStorage.setItem('token', resp.body.token);
    });
  });
  